# import sqlite3
import sqlite3
# import closing
from contextlib import closing

class DBHandler:

	def __init__(self, destination):
		# create the database
		self.db = sqlite3.connect(destination)
		
	def has_table(self, tablename):
		with closing(self.cursor()) as crs:
			try:
				# lookup the table name
				crs.execute("""
					SELECT COUNT(*)
					FROM sqlite_master
					WHERE name = (?)
					""", (tablename,))
				# check if it exists
				if crs.fetchone()[0] == 1:
					return True
				return False
			finally:
				crs.close()

	# returns a database cursor object
	def cursor(self):
		return self.db.cursor()

	# gets the last row id
	def get_last_id(self, table):
		with closing(self.cursor()) as crs:
			try:
				crs.execute("""SELECT id from {} 
							   ORDER BY id DESC 
							   LIMIT 1""".format(table));
				return crs.fetchone()[0]
			finally:
				crs.close()

	# delete a table from the database
	def delete_table(self, tablename):
		with closing(self.cursor()) as crs:
			try:
				# check if the table exists
				if self.has_table(tablename):
					# execute a drop command
					crs.execute("""
						DROP TABLE {}
						""".format(tablename))		
			finally:
				crs.close()

	# make a table
	def make_table(self, tablename, *heads):
		# check if table already exists
		with closing(self.cursor()) as crs:
			try:
				if not self.has_table(tablename):
					crs.execute('''
					CREATE TABLE {}(id INTEGER PRIMARY_KEY, {})
					'''.format(tablename, ", ".join(heads)))
				self.db.commit()
			finally:
				crs.close()

	# add an entry to a table
	def add_entries(self, table, entries):
		with closing(self.cursor()) as crs:
			try:
				values_unknown = "(?, {}?)".format("?," * (len(entries[0]) - 1))
				print values_unknown, heads
				print """
					INSERT INTO {} 
					VALUES {}""".format(table, heads, values_unknown)
				crs.executemany("""
					INSERT INTO {}({}) 
					VALUES {}""".format(table, heads, values_unknown), entries)
			finally:
				crs.close()

	# gets all entries from a table
	def get_all_entries(self, table):
		with closing(self.cursor()) as crs:
			try:
				crs.execute("""SELECT * FROM {}""".format(table))
				data = crs.fetchone()
				return crs.fetchone()
			finally:
				crs.close()