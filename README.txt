Contributor:	Robert Botarleanu
A wrapper over sqlite3 with the aim to provide an easy-to-use decorator for handling a database in python.

Class name: DBHandler
Methods:
	+ __init__('databasepath')	
		-> 'databasepath' should be ':memory:' for loading in RAM or a file

	+ make_table('tablename', *heads)
		-> tablename is the name of the table to be created
		-> *heads is a variable length list of the column heads

	+ delete_table('tablename')
		-> 'tablename' the name of the table to be deleted

	+ get_last_id('table')
		-> 'table' the name of the table of which to find the last insert id	

	+ get_all_entries('table')
		-> prints all entries of table

	+ cursor()
		-> used internally to provide a database cursor

	
